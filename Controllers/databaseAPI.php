<?php



namespace Controllers;

use PDO;
use PDOException;

abstract class Salt{
    const value = '123';
}

class databaseAPI
{
    protected $conn;
    public function __construct()
    {
        try {
            $this->conn = new PDO("mysql:host=localhost;dbname=lab5", "root", "passWORD");
//            echo "Database connection established";
        }
        catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }


    public function userExist($login,$email)
    {
        try {
            $query = "SELECT user_id from users WHERE login = :login or email = :email";
            $stmt = $this->conn->prepare($query);






            $stmt->bindParam(':login', $login);
            $stmt->bindParam(':email', $email);
            $stmt->execute();


            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if (count($result) > 0){
                return true;
            }
            else{
                return false;
            }
        } catch (PDOException $e) {
            echo "Error check: " . $e->getMessage();
        }
    }

    public function credentialsIsCorrect($login,$password)
    {
        try {
            $query = "SELECT login,password from users WHERE login = :login";
            $stmt = $this->conn->prepare($query);


            $stmt->bindParam(':login', $login);
            $stmt->execute();



            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $user = [];
            if (count($result) > 0){
                $row = $result[0];

                    $user = $row;
                    if (password_verify(Salt::value . $password, $user['password'])){
                        return true;
                    }
                }
            else{
                return false;
            }
        } catch (PDOException $e) {
            echo "Error check: " . $e->getMessage();
        }
    }
    public function createUser($login,$email,$password){
        try {
            $login = addslashes($login);
            $email = addslashes($email);
            $password = addslashes($password);

            $query = "INSERT INTO users (login,password,email) VALUES (:login,:password,:email)";
            $stmt = $this->conn->prepare($query);


            $hashPass = password_hash(Salt::value . $password,PASSWORD_DEFAULT);


            $stmt->bindParam(':login', $login);
            $stmt->bindParam(':password', $hashPass);
            $stmt->bindParam(':email', $email);
            $stmt->execute();

            $user_id = $this->conn->lastInsertId();
//            echo "user succes created. ID: ${user_id}";
            return $user_id;
        } catch (PDOException $e) {
            echo "Error creating new user: " . $e->getMessage();
        }
    }


    public function getUserId($login)
    {
        try {
            $query = "SELECT user_id from users WHERE login = :login";
            $stmt = $this->conn->prepare($query);

            $stmt->bindParam(':login', $login);
            $stmt->execute();


//            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            return reset($result);
        } catch (PDOException $e) {
            echo "Error check: " . $e->getMessage();
        }
    }
    public function changePassword($user_id,$newPassword)
    {
//        $user_id = $this->getUserId($login);
        try {
            $query = "UPDATE users SET password = :newPassword WHERE user_id = :user_id;";
            $stmt = $this->conn->prepare($query);


            $hashPass = password_hash(Salt::value . $newPassword,PASSWORD_DEFAULT);


            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':newPassword', $hashPass);
            $result = $stmt->execute();

            if ($result === true){
                return true;
            }
            else{
                return false;
            }
        } catch (PDOException $e) {
            echo "Error check: " . $e->getMessage();
        }
    }

    public function deleteAccount($login)
    {
        try {
            $query = "DELETE FROM users WHERE login = :login";
            $stmt = $this->conn->prepare($query);

            $stmt->bindParam(':login', $login);
            $result = $stmt->execute();

            if ($result === true){
                return true;
            }
            else{
                return false;
            }
        } catch (PDOException $e) {
            echo "Error check: " . $e->getMessage();
        }

    }

    public function getAllUsers()
    {
        try {
            $query = "SELECT * from users ";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();


            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "Error check: " . $e->getMessage();
        }
    }
}