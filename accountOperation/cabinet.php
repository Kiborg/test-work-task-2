<?php
session_start();
require_once '../Controllers/databaseAPI.php';
use Controllers\databaseAPI;
$db = new databaseAPI();

if (!(isset($_SESSION['isLogged']) and $_SESSION['isLogged'] === 'true')){
    header("Location: " . '../index.php');
}


$username = $_SESSION['username'];
$message = '';

if (isset($_POST['currentPassword']) and isset($_POST['newPassword']) and isset($_POST['repeatNewPassword'])){
    $password = $_POST['currentPassword'];
    $newPassword = $_POST['newPassword'];
    $repeatNewPassword = $_POST['repeatNewPassword'];

    if ($repeatNewPassword !== $newPassword){
        $message = 'new passwords are not equal. password is not changed';
    }
    else{
        if ($db->credentialsIsCorrect($username,$password)){
            $user_id = $db->getUserId($_SESSION['username']);
            $passwordChanged = $db->changePassword($user_id,$newPassword);

            if($passwordChanged)
                $message = 'succesfull change password';
        }else
            $message = 'current password is not right';

    }
}

if (isset($_GET['delete_account'])){
    $db->deleteAccount($username);
    session_destroy();
    header("Location: " . '../index.php');
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <style>
        body{
            background-color: darkkhaki;
        }
    </style>
</head>
<body>




<div class="container mt-2">
        <div class="row ">
            <div class="col-md-4">
                <a href="../welcome.php">
                    <button type="button" class="btn btn-primary">go back</button>
                </a>
            </div>
            <div id="changePasswordBlock" class="col-md-4 ">
                <form class="mt-2" method="post" action="cabinet.php?change_password=true">
                <div class="mb-5 mt-5">
                    <p class="text-start">Your login: <?=$username?></p>
                    <p class="text-start"><?=$message?></p>
                </div> 
                <div class="mt-3">
                    <label for="currentPassword" class="form-label">Current password</label>
                    <input name="currentPassword" id="currentPassword" class="form-control " type="password" aria-label="default input example">
                </div>
                <div class="mt-3">
                    <label for="newPassword" class="form-label">New password</label>
                    <input name="newPassword" id="newPassword" class="form-control" type="password" aria-label="default input example">
                </div>
                <div class="mt-3">
                    <label for="repeatNewPassword" class="form-label">Repeat new password</label>
                    <input name="repeatNewPassword" id="repeatNewPassword" class="form-control" type="password" aria-label="default input example">
                </div>
                    <button type="submit" class="btn btn-primary offset-7 mt-3">change password</button>
                </form>
            </div>
            <div class="col-md-4">
              <a href="cabinet.php?delete_account=true"><button type="submit"  class="btn btn-danger  offset-8 one">delete account</button></a>
            </div>
        </div>
    
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>
</html>