<?php
session_start();
$message = '';
require_once '../Controllers/databaseAPI.php';
use Controllers\databaseAPI;

if (isset($_SESSION['isLogged']) and $_SESSION['isLogged'] === 'true'){
    header("Location: " . 'welcome.php');
}

if (isset($_POST['username'])  and isset($_POST['email'])
    and isset ($_POST['password']) and isset ($_POST['repeatPassword'])){
    if ($_POST['password'] != $_POST['repeatPassword']){
        $message = 'password are not equal';
    }
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $db = new databaseAPI();

    if($db->userExist($username,$email)){
        $message = 'user already exist';
    }else{
        $new_user = $db->createUser($username,$email,$password);
        $success_login = $db->credentialsIsCorrect($username,$password);

        if($success_login){
            $_SESSION['isLogged'] = 'true';
            $_SESSION['username'] = $username;
            header("Location: " . '../welcome.php');
        }else{
            $message = 'error credentials';
        }
    }
}



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="registrationScript.js" defer></script>
    <style>
        body{
            background-color: darkkhaki;
        }
    </style>
</head>
<body>

<div class="registrationForm container" >
   
    <div class="row mt-5">
        
        <form class="col-4 offset-md-4" action="registration.php" method="post" enctype="multipart/form-data">
            <div class="text-center">
                <h5 >Registration</h5>
            </div>
            <div class="signMessage mb-2"><?=$message?></div>
            <!-- username and email -->

            <div class="email">
                <div class="mb-3">
                    <label for="username" class="form-label">username</label>
                    <input id="username" type="text" class="form-control">
                </div>
                <div class="mb-3 ">
                    <label for="email" class="form-label">email</label>
                    <input id="email" type="email" class="form-control"  placeholder="name@example.com">
                </div>
            </div>


            <!-- password block -->
            <div class="password">
                <label for="password" class="form-label">Password</label>
                <input id="password" type="password"  class="form-control" aria-describedby="passwordHelpBlock">
                <label for="repeatPassword" class="form-label">repeat your Password</label>
                <input id="repeatPassword" type="password"  class="form-control" aria-describedby="passwordHelpBlock">
            </div>




            <div class="container mt-3">
                <div class="row">
                    <div class="col-md-3 ">
                        <a href="../index.php"><button type="button" class="btn btn-primary">sign in</button></a>
                    </div>
                    <div class="col-md-3 offset-md-5">
                        <input class="btn btn-primary" type="button" value="registration" id="registrationButton">
                    </div>
                </div>
            </div>


        </form>


    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>
</html>