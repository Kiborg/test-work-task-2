const d = document.querySelector('#registrationButton')
d.addEventListener('click',(event)=>{

    const username = document.querySelector('#username').value;
    const email = document.querySelector('#email').value;
    const password = document.querySelector('#password').value;
    const repeatPassword = document.querySelector('#repeatPassword').value;

    const fieldsIsCorrect = (username != null && email != null && password != null && repeatPassword != null)

    if (password === repeatPassword && password.length > 2){
       fetch('../async.php',{
           method: 'POST',
           headers: {
               'Content-Type': 'application/json'
           },
           body: JSON.stringify({
               username: username,
               email: email,
               password: password,
               repeatPassword: repeatPassword
           })
       })
           .then(response => {
               if (response.ok) {
                   return response.json(); // Обрабатываем успешный ответ
               } else {
                   throw new Error('Network response was not ok.'); // Бросаем ошибку для обработки негативных случаев
               }
           })
           .then(json => {
               console.log(json.status)
               if (json.status == 'Success registration'){
                   window.location.href = '../index.php';
               }
           })
           .catch(error => {
               console.error('There was a problem with the fetch operation:', error); // Обработка ошибок
           });
   }
})

