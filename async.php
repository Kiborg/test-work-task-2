<?php
use Controllers\databaseAPI;

include 'Controllers/databaseAPI.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");

$message = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $credentials =  json_decode(file_get_contents('php://input'), true);
    if (isset($credentials['username']) && isset($credentials['email']) && isset($credentials['password']) && isset($credentials['repeatPassword'])) {
        if ($credentials['password'] != $credentials['repeatPassword']) {
            $message = 'Passwords do not match';
        } else {
            $username = $credentials['username'];
            $email = $credentials['email'];
            $password = $credentials['password'];

            $db = new databaseAPI();

            if ($db->userExist($username, $email)) {
                $message = 'User already exists';
            } else {
                $new_user = $db->createUser($username, $email, $password);
                $success_login = $db->credentialsIsCorrect($username, $password);

                if ($success_login) {
                    $message = 'Success registration';

                } else {
                    $message = 'Error during registration';
                }
            }
        }
    } else {
        $message = 'Incomplete data provided';
    }
} else {
    $message = 'Invalid request method';
}

echo json_encode(['status' => $message]);

