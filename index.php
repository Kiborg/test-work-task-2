<?php


use Controllers\databaseAPI;
require_once 'Controllers/databaseAPI.php';
session_start();
$message = '';

if (isset($_SESSION['isLogged']) and $_SESSION['isLogged'] === 'true'){
    header("Location: " . 'welcome.php');
}

if (isset($_POST['username']) and isset($_POST['password'])){
    $username = $_POST['username'];
    $password = $_POST['password'];
    
    $db = new databaseAPI();
    $success_login = $db->credentialsIsCorrect($username,$password);
    
    if($success_login){
        $_SESSION['isLogged'] = 'true';
        $_SESSION['username'] = $username;
        header("Location: " . 'welcome.php');
    }else{
        $message = 'uncorrect credentials';
    }
}




?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">

    <style>
        body{
            background-color: darkkhaki;
        }
    </style>
</head>
<body>

<div class="signIn container" >

    <div class="row mt-5">
        <form class="col-4 offset-md-4" action="index.php" method="post" >
            <div class="text-center">
                <h5>Sign in</h5>
            </div>
            <div class="signMessage mb-2"><?=$message?></div>
            <!-- username -->

            <div class="email">
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">username</label>
                    <input name="username" type="text" class="form-control" id="exampleFormControlInput1">
                </div>
            </div>


            <!-- password block -->
            <div class="password">
                <label for="inputPassword5" class="form-label">Password</label>
                <input name="password" type="password" id="inputPassword5" class="form-control" aria-describedby="passwordHelpBlock">
            </div>


            <div class="container mt-3">
                <div class="row">
                    <div class="col-md-3">
                        <a href="accountOperation/registration.php"><button type="button" class="btn btn-primary">registration</button></a>
                    </div>
                    <div class="col-md-3 offset-md-6">
                        <input class="btn btn-primary" type="submit" value="sign in">
                    </div>
                </div>
            </div>


        </form>
     


    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>
</html>