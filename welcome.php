<?php
session_start();
if (isset($_POST['unLogin']) and $_POST['unLogin'] == 'true'){
    session_destroy();
    header("Location: " . 'index.php');
}else{
    $message = 'Hello,'  . $_SESSION['username'];
}



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <style>
        body{
            background-color: darkkhaki;
        }


    </style>
    <script src="js/welcomScript.js" defer>
    </script>
</head>
<body>



<div class="container">
    <div class="row mt-2">
        <div class="col-md-4">
            <a href="accountOperation/cabinet.php">
                <button type="button" class="btn btn-primary position-relative">
                    Change credentials
                </button>
            </a>
        </div>

        <div class="col-md-4">
                <div id="buttonSeeAllUsers" class="position-relative py-2 px-4 text-bg-secondary border border-secondary rounded-pill">
                    some button <svg width="1em" height="1em" viewBox="0 0 16 16" class="position-absolute top-100 start-50 translate-middle mt-1" fill="var(--bs-secondary)" xmlns="http://www.w3.org/2000/svg"><path d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/></svg>
                </div>
        </div>

        <div class="col-md-4">
            <form class="position-relative start-100" action="welcome.php" method="post">
                <button type="submit" class="btn btn-primary" name="unLogin" value="true">exit</button>
            </form>
        </div>
    </div>


    <p class="message fs-2">
        <?=$message?>
    </p>







<div class="row offset-md-4 col-md-4">
<!--  <table id="output_section" class=" table table-striped-columns ">-->
<!--      <thead>-->
<!--      <tr>-->
<!--          <th scope="col">id</th>-->
<!--          <th scope="col">username</th>-->
<!--          <th scope="col">email</th>-->
<!--          <th scope="col">password</th>-->
<!--      </tr>-->
<!--      </thead>-->
<!--      <tbody>-->
<!---->
<!--      </tbody>-->
<!--  </table>-->
</div>


</div>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>
</html>
